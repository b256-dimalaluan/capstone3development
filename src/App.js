
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';

//import Banner from './components/Banner';
//import Highlights from './components/Highlights';
import React from 'react';
import { UserProvider } from './userContext';
import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import { useState, useEffect } from 'react';
import AdminDashboard from './pages/AdminDashboard';
import Order from './pages/Order';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import ErrorPage from './pages/ErrorPage';

function App() {


  let [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing the localStorage when a user logout

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect (() => {
    console.log(user);
    console.log(localStorage);
  })





  return (
    // Fragments are needed when there are two or more components, pages, or html elements
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
      {/* Self Closing Tags*/}
      <AppNavBar />
      <Container>
        <Routes>
          {/* path is the endpoint and element is the page*/}
          <Route path="/" element={<Home />} />
          <Route path="/products/active" element={<Products />} />
          <Route path="/productView/:productId" element={<ProductView />} />
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/logout" element={<Logout />} />
          <Route path="/admin" element={<AdminDashboard />} />
          <Route path="/order" element={<Order />} />
          <Route path="*" element={<ErrorPage />} />
        </Routes>
      </Container >



      </Router>
    </UserProvider>
  );
}

export default App;
