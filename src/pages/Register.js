import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../userContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {

	

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const navigate = useNavigate;
	const { user, setUser } = useContext(UserContext);
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);



	useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(password1 === password2){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [ email, password1, password2]);


	function registerUser(e) {

		e.preventDefault();
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({email: email, password: password1})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if (data === true) {

          localStorage.setItem('token', data.access)




          Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to Zeke's Store!"
          })
        } else {
          Swal.fire({
            title: "Duplicate email found",
            icon: "error",
            text: "Please provide a different email."
          })
        }

		})


		// clear input fields
		setEmail('');
    setPassword1('');
    setPassword2('');


	}

	return(
		(user.id !== null ) ?
      <Navigate to="/login"/>
      :
		 <Form onSubmit={e => registerUser(e)}>

	      <Form.Group className="mb-3" controlId="formBasicEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="formBasicPassword">
	        <Form.Label>Password</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password2} onChange={e => setPassword2(e.target.value)}/>
	      </Form.Group>
    


      
      
      {
      	isActive ?
      		<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
      	:
      		<Button variant="danger" type="submit" id="submitBtn" disabled >Submit</Button>
      }


      
    </Form>
	)

}