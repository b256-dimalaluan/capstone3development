import { Button, Form } from 'react-bootstrap';
import { useState, useContext, useEffect } from 'react';
import UserContext from '../userContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const navigate = useNavigate();

  console.log(email);
  console.log(password);

  useEffect(() => {
    setIsActive(email !== '' && password !== '');
  }, [email, password]);

  function loginUser(e) {
  e.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      email: email,
      password: password,
    }),
  })
    .then((res) => res.json())
    .then((data) => {
      if (typeof data.access !== 'undefined') {
        localStorage.setItem('token', data.access);

        setUser({ id: data.id });
        retrieveUserDetails(data.access); // Move this line to the next `.then` block
        Swal.fire({
          title: 'Login Successful',
          icon: 'success',
          text: "Welcome to Zeke's Store!",
        });
      } else {
        Swal.fire({
          title: 'Authentication failed',
          icon: 'error',
          text: 'Check your login credentials and try again later',
        });
      }
    })
    .catch((error) => {
      console.log('Login Error:', error);
    })
    .finally(() => {
      setEmail('');
      setPassword('');
    });
}





   const retrieveUserDetails = (token) => {
      fetch(`${process.env.REACT_APP_API_URL}/users/${user.id}/userDetails`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((res) => {
          if (!res.ok) {
            throw new Error('Failed to retrieve user details');
          }
          return res.json();
        })
        .then((data) => {
        if (data.isAdmin) {
          navigate('/admin');
        } 
        })
        .catch((error) => {
          console.log('Retrieve user details error:', error);
        });
    };


  return (
    <>
      {user.id !== null ? (
        <Navigate to="/products/active" />
      ) : (
        <Form onSubmit={loginUser}>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </Form.Group>

          <Button variant="success" type="submit" disabled={!isActive}>
            Login
          </Button>
        </Form>
      )}
    </>
  );
}
