import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';


	export default function ProductView() {

		

		const { user } = useContext(UserContext);
		const { productId, userId, quantity } = useParams();
		const navigate = useNavigate();

		const [name, setName] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState(0);

		useEffect(() => {

			console.log(productId);

			fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
			.then(res => res.json())
			.then(data => {

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);

			})
		}, [productId]);



		return(
			<Container className="mt-5">
				<Row>
					<Col lg={{ span: 6, offset: 3 }}>
						<Card>
							<Card.Body className="text-center">
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PhP {price}</Card.Text>
								<Card.Subtitle>Class Schedule</Card.Subtitle>
								<Card.Text>8 am - 5 pm</Card.Text>
								{user.id !== null ? (
									  <Button variant="primary" as={Link} to={'/order'}>
									    Order
									  </Button>

									) : (

									  <Link className="btn btn-danger btn-block" to="/login">
									    Login
									  </Link>
								)}
								
							</Card.Body>		
						</Card>
					</Col>
				</Row>
			</Container>
		)
	}