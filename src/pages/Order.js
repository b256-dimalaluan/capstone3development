import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

import 'bootstrap/dist/css/bootstrap.min.css';


export default function Order() {
  const { user } = useContext(UserContext);
  const { userId, productId } = useParams();
  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);
  const [quantity, setQuantity] = useState(1);

  const handleDecrement = () => {
    if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  const handleIncrement = () => {
    // Adjust the condition for the maximum allowed quantity if needed
    setQuantity(quantity + 1);
  };

  const handleCheckout = () => {
    setLoading(true);

    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        userId,
        productId,
        quantity
      })
    })
      .then(res => res.json())
      .then(data => {
        setLoading(false);

        if (data === true) {
          Swal.fire({
            title: 'Successfully Ordered!',
            icon: 'success',
            text: 'You have ordered.'
          });

          navigate('/products/active');
        } else {
          Swal.fire({
            title: 'Something went wrong!',
            icon: 'error',
            text: 'Please try again'
          });
        }
      })
      .catch(error => {
        setLoading(false);
        console.log('Checkout Error:', error);
      });
  };

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>Order Details</Card.Title>
              <Card.Subtitle>User ID: {user.id}</Card.Subtitle>
              <Card.Subtitle>Product ID: {productId}</Card.Subtitle>
               <Form.Group className="mt-3">
                <Form.Label>Quantity:</Form.Label>
                <div className="input-group">
                  <span className="input-group-btn">
                    <button
                        type="button"
                        className="btn btn-outline-secondary btn-number"
                        onClick={handleDecrement}
                    >
                      <span className="glyphicon glyphicon-minus"></span>
                    </button>
                  </span>
                  <input
                    type="text"
                    name="quant[1]"
                    className="form-control input-number"
                    value={quantity}
                    min="1"
                    max="10" // Adjust the maximum allowed quantity if needed
                    readOnly
                  />
                  <span className="input-group-btn">
                    <button
                      type="button"
                      className="btn btn-outline-secondary btn-number"
                      onClick={handleIncrement}
                    >
                      <span className="glyphicon glyphicon-plus"></span>
                    </button>
                  </span>
                </div>
              </Form.Group>

              {user.id !== null ? (
                <Button variant="primary" onClick={handleCheckout} disabled={loading}>
                  {loading ? 'Processing...' : 'Place Order'}
                </Button>
              ) : (
                <Button variant="danger" onClick={() => navigate('/login')}>
                  Login to Place Order
                </Button>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
