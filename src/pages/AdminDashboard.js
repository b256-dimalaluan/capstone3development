// Admin Dashboard
// admins should be able to: 

// Create a Product ( localhost:4000/products/create )
// Retrieve all products ( localhost:4000/products/all )
// Update Product Information ( localhost:4000/products/update/${product.id} )
// Deactivate/reactivate product ( localhost:4000/products/64410bd3a761623e3fb2d7ad/archive )


import React, { useState, useEffect } from 'react';

const AdminDashboard = () => {
  const [products, setProducts] = useState([]);
  const [newProduct, setNewProduct] = useState({
    name: '',
    price: '',
    description: '',
  });

  useEffect(() => {
    fetchProducts();
  }, []);


  // Retrieve all products ( localhost:4000/products/all )
  const fetchProducts = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/all`);
      const productsData = await response.json();
      setProducts(productsData);
    } catch (error) {
      console.log('Error fetching products:', error);
    }
  };

  // Create a Product ( localhost:4000/products/create )
  const createProduct = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newProduct),
      });

      // Handle the response as needed
      if (response.status === 200) {
        // Product created successfully
        setNewProduct({
          name: '',
          price: '',
          description: '',
        });
      } else {
        // Handle error cases
      }

      // Fetch the updated list of products
      fetchProducts();
    } catch (error) {
      console.log('Error creating product:', error);
    }
  };

  const handleInputChange = (e) => {
    setNewProduct({
      ...newProduct,
      [e.target.name]: e.target.value,
    });
  };

  // Update Product Information ( localhost:4000/products/update/${product.id} )
  const updateProduct = async (productId) => {
  try {
    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name: newProduct.name,
        price: newProduct.price,
        description: newProduct.description,
      }),
    });

    // Handle the response as needed
    if (response.status === 200) {
      // Product updated successfully
      console.log('Product updated successfully');
    } else {
      // Handle error cases
      console.log('Error updating product');
    }

    // Fetch the updated list of products
    fetchProducts();
  } catch (error) {
    console.log('Error updating product:', error);
  }
};

  return (
    <div>
      <h2>Create Product</h2>
      <form onSubmit={createProduct} style={{ display: 'flex', flexDirection: 'row' }}>
      <input
        type="text"
        placeholder="Name"
        name="name"
        value={newProduct.name}
        onChange={handleInputChange}
        style={{ marginRight: '10px' }}
      />
      <input
        type="text"
        placeholder="Price"
        name="price"
        value={newProduct.price}
        onChange={handleInputChange}
        style={{ marginRight: '10px' }}
      />
      <textarea
        placeholder="Description"
        name="description"
        value={newProduct.description}
        onChange={handleInputChange}
        style={{ marginRight: '10px' }}
      ></textarea>
      <button type="submit" style={{ backgroundColor: 'blue', color: 'white' }}>
        Create
      </button>
    </form>

      <h2>All Products</h2>
      <ul>
        {products.map((product) => (
          <li key={product.id}>
            <h3>{product.name}</h3>
            <p>Price: {product.price}</p>
            <p>Description: {product.description}</p>
            <button
              onClick={() => {
                updateProduct(product.id);
              }}
            >
              Update
            </button>
            <button
              onClick={() => {
                // Logic for activating/deactivating product
              }}
            >
              {product.isActive ? 'Deactivate' : 'Reactivate'}
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default AdminDashboard;
