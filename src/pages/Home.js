import Banner from '../components/Banner.js';
import Highlights from '../components/Highlights';

export default function Home() {
	return(
		<>
			<Banner />
			<Highlights />

		</>
	)
}