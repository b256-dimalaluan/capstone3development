import { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';

export default function Products () {
	const [ products, setProducts ] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/active`, {
        method: "GET",
        headers: {
          'Content-Type': 'application/json'
        } 
      })
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(
			  data.map((product) => (
			    <ProductCard key={product.id} productProp={product}/>
			  ))
			);

		})},[]);

	

	return (
		<>
			<h1>Products</h1>
			{products}
		</>
	)
}