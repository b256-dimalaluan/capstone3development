import {Row, Col, Card} from 'react-bootstrap'; 

export default function Highlights () {
	return (
		<Row className="mt-3 mb-3">
			<Col>
				<Card className="cardHighlight p-3">
	      			<Card.Body>
        				<Card.Title className="text-center">
        					<h2>Shop from Home</h2>
        				</Card.Title>
				        <Card.Text>
				          We are excited to offer you the opportunity to shop from the comfort of your own home. With just a few clicks, you can browse our extensive collection of products and easily make your purchase without ever leaving your house. We understand that convenience is key, and our goal is to make your shopping experience as easy and enjoyable as possible. Whether you're in the market for new clothes, electronics, or household items, we have everything you need to make your home shopping experience a success. So sit back, relax, and start exploring all that we have to offer - we're confident you'll find something you love!
				        </Card.Text>
        		
		      	    </Card.Body>
		    	</Card>
		    </Col>

		    {/*<Col xs={12} md={4}>
		    	<Card className="cardHighlight p-3">
	      			<Card.Body>
        				<Card.Title>
        					<h2>Study Now, Pay Later</h2>
        				</Card.Title>
				        <Card.Text>
				         Ex Lorem cillum consequat ad. Consectetur enim sunt amet sit nulla dolor exercitation est pariatur aliquip minim. Commodo velit est in id anim deserunt ullamco sint aute amet. Adipisicing est Lorem aliquip anim occaecat consequat in magna nisi occaecat consequat et. Reprehenderit elit dolore sunt labore qui.
				        </Card.Text>
		      	    </Card.Body>
		    	</Card>
		    </Col>
		    <Col xs={12} md={4}>
		    	<Card className="cardHighlight p-3">
	      			<Card.Body>
        				<Card.Title>
        					<h2>Be Part of Our Community</h2>
        				</Card.Title>
				        <Card.Text>
				         Minim nostrud dolore consequat ullamco minim aliqua tempor velit amet. Officia occaecat non cillum sit incididunt id pariatur. Mollit tempor laboris commodo anim mollit magna ea reprehenderit fugiat et reprehenderit tempor. Qui ea Lorem dolor in ad nisi anim. Culpa adipisicing enim et officia exercitation adipisicing.
				        </Card.Text>
		      	    </Card.Body>
		    	</Card>
		    </Col>*/}

		</Row>
	)
}