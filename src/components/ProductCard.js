import { useState } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({productProp}) {

    // console.log(props);
    // console.log(typeof props);
    // Object Deconstruction
    const { name, description, price, _id } = productProp;

    
 
    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Button variant="primary" as={Link} to={`/productView/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    )
}
