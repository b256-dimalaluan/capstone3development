import { Button, Row, Col } from 'react-bootstrap';



export default function Banner() {
	return(
		<Row>
			<Col className="p-5 text-center">
				<h1>Zeke's Store</h1>
				<p>Buy mechanical tools without leaving your home!</p>
				<Button variant="primary">Register your account now!</Button>
			</Col>
		</Row>
	)
}