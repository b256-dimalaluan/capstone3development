import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../userContext';

export default function AppNavBar() {

	

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
      		<Container>
		        <Navbar.Brand as={Link} to="/">Zeke's Store</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
          			<Nav className="me-auto">
            			<Nav.Link as={NavLink} to="/products/active">Products</Nav.Link>
            			{
            				(user.id !== null) ?
            				<>
            					{user.isAdmin === true && (
				                  <Nav.Link as={NavLink} to="/admin">
				                    Admin
				                  </Nav.Link>
				                )}
            					<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
            					<Nav.Link as={NavLink} to="/order">Order</Nav.Link>
            				</>	
            				:
            				<>
            					<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            					<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            				</>
            			}
            			
		            </Nav>
		        </Navbar.Collapse>
		     </Container>
	    </Navbar>
	)
}